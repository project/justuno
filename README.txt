-- SUMMARY --

Justuno is the premier social marketing tool to grow your social and email
leads and convert visitors into customers via a simple instant offer widget
on your site. In exchange for a coupon code, customers must either
Facebook Like, Twitter Follow, Google+1, Join your mailing list or more
options all without ever having to leave your website.

To understand how Justuno works it’s best to just see him in action Please
visit our site to experience how you can start converting your own website
visitors today!

Key Features:
-Present instant offers in exchange for a Social or email lead capture
-Convert up to 3% of your daily website traffic
-Increase sales conversion by up to 20%
-Easy to set-up and manage. No technical skills needed.
-Available in any language

Read more at http://www.justuno.com

-- REQUIREMENTS --

Drupal 7

-- INSTALLATION --

Install as usual, see
https://drupal.org/documentation/install/modules-themes/modules-7
for further information.

-- CONFIGURATION --

Save module settings then follow "Justuno dashboard" link to configure widget.

-- CONTACT --

Erik Christiansen - http://drupal.org/user/2669637
